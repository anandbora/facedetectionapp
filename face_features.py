import cv2
import numpy as np
import dlib
import time
# from deepface import DeepFace

KNOWN_DISTANCE = 45 #INCHES
time_flag = False
reset = True
FACE_WIDTH = 5.9 #INCHES
cap = cv2.VideoCapture(0)
hog_face_detector = dlib.get_frontal_face_detector()
dlib_facelandmark = dlib.shape_predictor("/Users/gokulkumar/PycharmProjects/JossBox/shape_predictor_68_face_landmarks.dat")
face_cascade = cv2.CascadeClassifier('/Users/gokulkumar/PycharmProjects/JossBox/haarcascade_frontalface_default.xml')

def ref_face_width():
    img = cv2.imread('/Users/gokulkumar/PycharmProjects/JossBox/reference_face.png')
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    # print(faces)
    return faces[0][2]

def focal_length_finder (measured_distance, real_width, width_in_rf):
    focal_length = (width_in_rf * measured_distance) / real_width

    return focal_length

# distance finder function
def distance_finder(focal_length, real_object_width, width_in_frmae):
    distance = (real_object_width * focal_length) / width_in_frmae
    return distance

focal_length = focal_length_finder(KNOWN_DISTANCE,FACE_WIDTH,ref_face_width())

while True:
    face_count = 0
    views = 0
    ots = 0
    imp = 0
    ret, img = cap.read()
    frame_width = img.shape[1]
    frame_height = img.shape[0]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # cv2.imshow("Image", gray)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    # result = DeepFace.analyze(img,actions=['emotion','age', 'gender'],enforce_detection=False)
    # print(result)
    faces_landmark = hog_face_detector(gray)
    for face in faces_landmark:
        # print(face)
        face_landmarks = dlib_facelandmark(gray, face)
        #drawing face_landmarks
        for n in range(0, 68):
            x = face_landmarks.part(n).x
            y = face_landmarks.part(n).y
            cv2.circle(img, (x, y), 1, (0, 255, 255), 2)
    #logic to calculate head pose
    nose_top = np.array((face_landmarks.part(27).x, face_landmarks.part(27).y), dtype="double")
    nose_bottom = np.array((face_landmarks.part(30).x, face_landmarks.part(30).y), dtype="double")
    # print(nose_top[0])
    # print(nose_bottom)
    angle = np.arctan((nose_top[0] - nose_bottom[0])/(nose_top[1] - nose_bottom[1]))
    angle = angle * 180
    face_count = len(faces)
    # print(result['dominant_emotion'],result["age"],result['gender'])
    if face_count == 0:
        time_flag = False
    for (x, y, w, h) in faces:
        if time_flag == False:
            start = time.time()
            time_flag = True
        stop = time.time()
        ratio = (w*h)/(frame_width*frame_height)
        area = w*h
        # tried to fit a polynomial function using area, to find distance from cam but didnt work well
        # distance = (6.74809*(10**-8)*area*area) - (9.95765*(area/1000)) + 328.95
        distance = distance_finder(focal_length,FACE_WIDTH,w) * 2.54
        cv2.rectangle(img, (x, y), (x+w, y+h),(255,  0, 0), 3)
        # cv2.putText(img,str(result['dominant_emotion']) + str(result["age"]) + str(result['gender']),(200,100),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255))
        cv2.putText(img, str(w*h), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255))
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
    if angle>= -90 and angle<=90 :
        ots+=1
    if angle>= -45 and angle<=45 :
        imp+=1
    if angle>= -30 and angle<=30 and distance > 100 :
        views+=1
    attention_time = stop - start
    if face_count == 0:
        attention_time = 0
    cv2.putText(img, "Head angle : "+ str(round((angle),2)), (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0),2)
    cv2.putText(img, "Distance : " + str(round((distance),2)), (50, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0),2)
    cv2.putText(img, "Count : " + str(face_count), (50, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0),2)
    cv2.putText(img, "OTS : " + str(ots), (50, 200), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    cv2.putText(img, "Impression : " + str(imp), (50, 250), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    cv2.putText(img, "Views : " + str(views), (50, 300), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    cv2.putText(img, "Attention Time : " + str(round(attention_time,2)), (50, 350), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    # cv2.putText(img, "Emotion : " + str(result['dominant_emotion']), (50, 200), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0),2)
    # cv2.putText(img, "Age : " + str(result["age"]), (50, 250), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0),2)
    # cv2.putText(img, "Gender : " + str(result['gender']), (50, 300), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0),2)
    cv2.imshow("Final",img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()



